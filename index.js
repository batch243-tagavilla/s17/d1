// console.log("Hello")

// [Section] Function

    // Function
        // Functions in JS are lines/blocks of codes that tell our device/application to perform a certain task when called or invoke.
        // Functions are mostly created to create a complicated tasks to run several lines of code in succession.
        // They are also used to prevent repeating lines/block of codes that perform the same task  or function.

    // Function Declaration
        // Function statements - defines a function with specified parameters.
        // Syntax
            /* 
                function functionName () {
                    code block
                }    
            */

            // function - used to define a javascript functions.
            // functionName - functions are name to be able to use later in the code.
            // function block ( {} ) - statements which comprise the body of the function. This is where the code will be executed.
            
            function printName() {
                console.log("My name is John.")
                console.log("My last name is Dela Cruz.")
            }

    // Function Invocation
        // The code block and statements inside a function is not immediately executed when the function is defined or declared. The code block and statements inside a function is executed when the function is invoked/
        // It is common to use the term "call a function" instead of "invoke a function".

            printName();
            // functions are reusable.
            printName();

// [Section] Function Declaration and Function Expression

    // Function declaration
            // A function can be created through function declaration by using the keyword function and adding function name.

            // Declared functions are not executed immediately.

            declaredFunction();

            function declaredFunction() {
                console.log("Hello World from declaredFunction")
            }

    // Function expression
            // A function can also be stored in a variable. This is called function expression.

            // Anonymous function - function without a name.

            // variableFunction();
        
            let variableFunction = function() {
                console.log("Hello from variableFunction")
            }

            variableFunction();


            let funcExpression = function funcName() {
                console.log("Hello from funcExpression")
            }

            funcExpression();

    // You can reassign declared functions and function expression to new anonymous function.

            declaredFunction = function() {
                console.log("Updated declaredFunction")
            }

            declaredFunction();

            funcExpression = function() {
                console.log("updated funcExpression")
            }

            funcExpression();

            // Function expression using const keyword
            const constantFunc = function() {
                console.log("Initiated with const")
            }

            constantFunc();

            // Error
            // constantFunc = function() {
            //     console.log("Cannot be reassigned")
            // }

// [Section] Function Scoping

    /* 
        Scope is the accessibility of variable within our program.

        JS variables, it has 3 types of scopes:
        1. local/block scope
        2. global scope
        3. function scope

    */

        {
            let localVar = "Armando Perez";
            console.log(localVar);
        }

    let globalVar = "Mr. Worldwide";

    // function scope
    
    function showNames() {
        let functionLet = "Jane";
        const functionConst = "John";

        console.log(functionLet);
        console.log(functionConst);
    }

    showNames();
    
    // Error
    // console.log(functionLet);
    // console.log(functionConst);

// [Section] Nested functions 

    // You can create another function inside a function

    function myNewFunction() {
        let name = "Chris";

        console.log(name);
        function nestedFunction() {
            let nestedName = "George";

            console.log(nestedName);
            console.log(name);
        }

        nestedFunction();
    }

    myNewFunction();

    // Function and Global Scoped Variables
    let globalName = "Alexandro"

    function myNewFunction2() {
        let nameInside = "Renz";

        console.log(globalName);
        console.log(nameInside);
    }

    myNewFunction2();

// [Section] Using alert

    // alert("Hello World");

    function showSampleAlert() {
        alert("Hello World");
    }

    // showSampleAlert();

    // Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [Section] Prompt

    // let samplePrompt = prompt("Enter your name: ");
    // console.log(samplePrompt);
    // console.log(typeof samplePrompt);

    // let sampleNullPrompt = prompt("Dont enter anything");
    // console.log(sampleNullPrompt);
    // console.log(typeof sampleNullPrompt);
    // console.log((typeof sampleNullPrompt) === Object);

    function printWelcomeMessages() {
        let firstName = prompt("Enter your first name: ");
        let lastName = prompt("Enter your last name: ");

        console.log("Hello, " + firstName + " " + lastName + "!");
    }

    // printWelcomeMessages();

// [Section] Function Naming Convention

    // Function names should be definitive of the task it will perform. It usually contains a verb.

        function getCourses() {
            let course = ["Science 101", "Math 101", "English 101"]
        }

    // Avoid generic names to avoid confusion within your coed/program

        function get() {
            let name = "Jamie";
        }

    // Avoid pointless and inappropriate function names.

        function foo() {

        }

    // Name you function in small caps. Follow camelCasing.

        function displayCarInfo() {
            
        }